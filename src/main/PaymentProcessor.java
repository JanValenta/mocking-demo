/**
 * @author jvalenta
 * @since 2016-01-21
 */
public class PaymentProcessor {

    private MyPaymentGateway paymentGateway;

    public PaymentProcessor(MyPaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public void processPayment(PaymentData paymentData) {
        PaymentInputDTO paymentInputDTO = PaymentDataAdapter.preparePaymentData(paymentData);
        PaymentResultDTO paymentResultDTO = null;
        while (paymentResultDTO == null) {
            String credentials = CredentialsProvider.getInstance().getCredentials(paymentGateway.getName());
            paymentResultDTO = paymentGateway.process(paymentInputDTO, credentials);
        }

        // do some stuff with result
    }
}
