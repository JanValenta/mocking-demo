/**
 * @author jvalenta
 * @since 2016-01-21
 */
public class CredentialsProvider {

    private static CredentialsProvider instance = new CredentialsProvider();

    public static CredentialsProvider getInstance() {
        return instance;
    }

    private CredentialsProvider() {
    }

    public String getCredentials(String paymentProcessorName) {
        return paymentProcessorName+":some:Credentials";
    }
}
