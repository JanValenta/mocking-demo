/**
 * TODO: Enter a paragraph that summarizes what the class does and why someone might want to utilize it
 * <p>
 * � 2016 NetSuite Inc.
 *
 * @author jvalenta
 * @since 2016-01-21
 */
public interface PaymentGateway {

    PaymentResultDTO process(PaymentInputDTO paymentInputDTO, String credentials);

    String getName();
}
