/**
 *
 * @author jvalenta
 * @since 2016-01-22
 */
public class MockitoExamplePaymentGateway implements PaymentGateway {

    PaymentResultProvider paymentResultProvider;

    public MockitoExamplePaymentGateway(PaymentResultProvider paymentResultProvider) {
        this.paymentResultProvider = paymentResultProvider;
    }

    @Override
    public PaymentResultDTO process(PaymentInputDTO paymentInputDTO, String credentials) {
        if(paymentInputDTO.getCreditCard() == null) {
            return null;
        }
        return paymentResultProvider.getResult(new GatewayResult());
    }

    @Override
    public String getName() {
        return "Mockito Example";
    }
}
