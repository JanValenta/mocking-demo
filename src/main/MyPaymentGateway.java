/**
 *
 * @author jvalenta
 * @since 2016-01-21
 */
public class MyPaymentGateway implements PaymentGateway {

    private int invocationsCount = 0;

    public PaymentResultDTO process(PaymentInputDTO paymentInputDTO, String credentials) {
        // simulates GW communication error
        if(getInvocationCount(invocationsCount) == 0) {
            invocationsCount++;
            return null;
        }
        return new PaymentResultDTO();
    }

    private int getInvocationCount(int invocationsCount) {
        // som complex code
        return invocationsCount;
    }

    @Override
    public String getName() {
        return "MyPaymentGateway";
    }
}
