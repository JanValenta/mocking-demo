/**
 *
 * @author jvalenta
 * @since 2016-01-21
 */
public class PaymentInputDTO {
    private String creditCard;

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }
}
