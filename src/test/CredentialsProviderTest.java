import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author jvalenta
 * @since 2016-01-21
 */
public class CredentialsProviderTest {

    @Test
    public void testGetCredentials() throws Exception {
        CredentialsProvider credentialsProvider = CredentialsProvider.getInstance();
        assertEquals("Test:some:Credentials", credentialsProvider.getCredentials("Test"));
    }
}