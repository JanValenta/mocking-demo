import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnit44Runner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * https://github.com/jayway/powermock/wiki/MockitoUsage
 *
 * @author jvalenta
 * @since 2016-01-21
 */
@RunWith(PowerMockRunner.class)
// POWERMOCK - prepare classes for testing which needs to be byte-code manipulated. This includes final classes,
// classes with final, private, static or native methods that should be mocked and also classes that should be
// return a mock object upon instantiation.
@PrepareForTest({PaymentDataAdapter.class, CredentialsProvider.class})
public class PaymentProcessorTest {

    public static final String CREDIT_CARD_NO = "41111111111111111";
    @Mock
    MyPaymentGateway paymentGateway;

    @Mock
    PaymentResultDTO paymentResultDTO;

    @Mock
    PaymentInputDTO paymentInputDTO;

    @Mock
    PaymentData paymentData;

    @InjectMocks
    PaymentProcessor paymentProcessor;

    @Before
    public void setUp() throws Exception {
        // MOCKITO
        // define mocks functionality
        // return value if method called without arguments
        when(paymentInputDTO.getCreditCard()).thenReturn(CREDIT_CARD_NO);
        // return value/mock if method called with arguments,
        // arguments can be specify explicitly or by any(class) or anyXXX() mockito functions
        // first call of this function returns null, second call moc object, atd..
        // return value can be defined by input value
        when(paymentGateway.process(any(PaymentInputDTO.class), anyString())).thenReturn(null).thenReturn(paymentResultDTO);
        // use Mock as a Stub, call real method of the mocked object, keeps possibility to verify mock behavior
        when(paymentGateway.getName()).thenCallRealMethod();

        // POWERMOCK
        // mock all the static methods in a class
        mockStatic(PaymentDataAdapter.class);
        when(PaymentDataAdapter.preparePaymentData(any(PaymentData.class))).thenReturn(paymentInputDTO);
    }

    @Test
    public void testProcessPayment() throws Exception {
        paymentProcessor.processPayment(paymentData);

        // MOCKITO
        // paymentGateway.process was called twice with paymentInputDTO argument
        verify(paymentGateway, times(2)).process(paymentInputDTO, "MyPaymentGateway:some:Credentials");
    }

    @Test
    public void testProcessPaymentSingletonMock() throws Exception {
        when(paymentGateway.getName()).thenReturn("Mocked");

        PowerMockito.mockStatic(CredentialsProvider.class);
        PowerMockito.when(CredentialsProvider.getInstance()).thenReturn(mock(CredentialsProvider.class));

        // MOCKITO
        // arguments can be used for response
        // JAVA 8
        when(CredentialsProvider.getInstance().getCredentials(anyString())).thenAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            String name = (String) args[0];
            return name + "IHaveModifiedThisArgument";
        });
        // JAVA 6
//        when(CredentialsProvider.getInstance().getCredentials(anyString())).thenAnswer(new Answer<String>() {
//            @Override
//            public String answer(InvocationOnMock invocation) throws Throwable {
//                Object[] args = invocation.getArguments();
//                String name = (String) args[0];
//                return name + "IHaveModifiedThisArgument";
//            }
//        });

        paymentProcessor.processPayment(paymentData);

        // MOCKITO
        // paymentGateway.process was called twice with paymentInputDTO argument
        verify(paymentGateway, times(2)).process(paymentInputDTO, "MockedIHaveModifiedThisArgument");
    }
}