import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 *  Simple MOCKITO example
 *
 * @author jvalenta
 * @since 2016-01-22
 */
@RunWith(MockitoJUnitRunner.class)
public class MockitoExamplePaymentGatewayTest {

    public static final String CREDIT_CARD_NO = "41111111111111111";

    @Mock
    PaymentResultProvider paymentResultProvider;

    @Mock
    PaymentInputDTO paymentInputDTO;

    @Mock
    PaymentResultDTO paymentResultDTO;

    @InjectMocks
    MockitoExamplePaymentGateway paymentGateway;

    @Before
    public void setUp() throws Exception {
        // Initialize mocks, injects mocks into paymentGateway
        MockitoAnnotations.initMocks(this);

        when(paymentInputDTO.getCreditCard()).thenReturn(CREDIT_CARD_NO);

        // MOCKITO custom matcher example, argThat consumes org.hamcrest.Matcher
        when(paymentResultProvider.getResult(argThat(new MyMatcher()))).thenReturn(paymentResultDTO);
    }

    @Test
    public void testProcess() throws Exception {
        assertEquals(paymentResultDTO, paymentGateway.process(paymentInputDTO, null));
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals("Mockito Example", paymentGateway.getName());
    }

    class MyMatcher extends ArgumentMatcher<GatewayResult> {

        @Override
        public boolean matches(Object argument) {
            return argument instanceof GatewayResult;
        }
    }
}