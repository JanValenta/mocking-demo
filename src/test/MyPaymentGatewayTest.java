import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

/**
 *
 * @author jvalenta
 * @since 2016-01-22
 */
@RunWith(PowerMockRunner.class)
// POWERMOCK - prepare classes for testing which needs to be byte-code manipulated. This includes final classes,
// classes with final, private, static or native methods that should be mocked and also classes that should be
// return a mock object upon instantiation.
@PrepareForTest({MyPaymentGateway.class})
public class MyPaymentGatewayTest {

    @Mock
    PaymentInputDTO paymentInputDTO;

    @Test
    public void testProcess() throws Exception {
        // POWERMOCK
        // mock all the private methods in a class
        PaymentGateway paymentGatewaySpy = spy(new MyPaymentGateway());

        when(paymentGatewaySpy, method(MyPaymentGateway.class, "getInvocationCount", int.class)).withArguments(anyInt()).thenReturn(1);

        paymentGatewaySpy.process(paymentInputDTO, null);
    }

    @Test
    public void testGetName() throws Exception {
        PaymentGateway paymentGateway = new MyPaymentGateway();
        assertEquals("MyPaymentGateway", paymentGateway.getName());
    }
}