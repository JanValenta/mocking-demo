#!/bin/sh

JAVAC=/opt/jdk1.8/bin/javac

CLASSPATH="$CLASSPATH:java/lib/google-collections-0.9.jar"
CLASSPATH="$CLASSPATH:java/lib/gson-2.4.jar"
CLASSPATH="$CLASSPATH:java/lib/hamcrest-all-1.0.jar"
CLASSPATH="$CLASSPATH:java/lib/jsr305-1.3.9.jar"
CLASSPATH="$CLASSPATH:java/lib/junit-4.7.jar"
CLASSPATH="$CLASSPATH:java/lib/mockito-all-1.9.5.jar"
CLASSPATH="$CLASSPATH:java/lib/powermock-mockito-1.6.2-full.jar"

CLASSPATH="$CLASSPATH:java/main"
CLASSPATH="$CLASSPATH:java/test"

export CLASSPATH

find java/ -name '*.java' | xargs $JAVAC
